<?php
declare(strict_types=1);
/**
* CLASS TOOL_COMMON
* Add basic methods for general use in tools
*/
class tool_common {



	public $name;
	public $config;
	// string section_tipo
	public $section_tipo;
	// string section_id
	public $section_id;



	/**
	* __CONSTRUCT
	* @param string|int $section_id
	* @param string $section_tipo
	* @return void
	*/
	public function __construct(string|int|null $section_id, string $section_tipo) {

		// set tool name as class name
		$this->name = get_called_class();

		//set
		$this->section_tipo	= $section_tipo;
		$this->section_id	= $section_id;
	}//end __construct



	/**
	* GET_JSON
	* Gets tool context
	* This function to preserve calls coherence,
	* but only is used to get context, not data.
	* @param object|null $options = null
	* @return object $json
	*/
	public function get_json(object $options=null) : object {

		// options
			$get_context	= $options->get_context ?? true;
			$get_data		= $options->get_data ?? false;

		// JSON object
			$json = new stdClass();
				if (true===$get_context) {
					$json->context = [$this->get_structure_context()];
				}

		return $json;
	}//end get_json



	/**
	* GET_STRUCTURE_CONTEXT
	* Parse the tool context
	* Used when tools area loaded from different window
	* like time_machine do
	* The context data is stored in section 'dd1324' (Registered tools)
	* preparsed into the component_json 'dd1353' as JSON 'simple_tool_obj'
	* when tools are registered
	* @return dd_object $dd_object
	*/
	public function get_structure_context() : dd_object {

		// check valid name
			if ($this->name==='tool_common') {
				throw new Exception("Error. Tool name is wrong. Check your tool call using tool model", 1);
			}

		// tool name. Fixed on construct
			$name = $this->name;

		// component dato simple_tool_obj (dd1353)
			$component_tipo			= tools_register::$simple_tool_obj_component_tipo;
			$model					= RecordObj_dd::get_modelo_name_by_tipo($component_tipo,true);
			$simple_tool_component	= component_common::get_instance(
				$model,
				$component_tipo,
				$this->section_id,
				'list',
				DEDALO_DATA_NOLAN,
				$this->section_tipo
			);
			$simple_tool_obj_dato	= $simple_tool_component->get_dato();
			$tool_object			= reset($simple_tool_obj_dato);
			// tool_object check
			if (empty($tool_object)) {
				debug_log(__METHOD__
					. " Error. Invalid tool_object. Unable to continue !  " . PHP_EOL
					. ' model: '.to_string($model) .PHP_EOL
					. ' component_tipo: '.to_string($component_tipo) .PHP_EOL
					. ' section_tipo: '.to_string($this->section_tipo) .PHP_EOL
					. ' section_id: '.to_string($this->section_id) .PHP_EOL
					, logger::ERROR
				);
			}

		// label. (JSON list) Try match current lang else use the first lang value
			$tool_label = array_find($tool_object->label, function($el){
				return $el->lang===DEDALO_DATA_LANG;
			})->value ?? reset($tool_object->label)->value;

		// developer
			$developer_data = array_find($tool_object->developer, function($el){
				return $el->lang===DEDALO_DATA_NOLAN;
			});
			$developer = !empty($developer_data)
				? $developer_data->value
				: [];

		// description. (text_area) Try match current lang else use the first lang value
			$description = array_find((array)$tool_object->description, function($el){
				return $el->lang===DEDALO_DATA_LANG;
			})->value[0] ?? reset($tool_object->description)->value[0];

		// labels. take care of empty objects like '{}'
			$labels = [];
			if(!empty($tool_object->labels) && !empty((array)$tool_object->labels)) {

				// add label with lang fallback
				foreach ($tool_object->labels as $current_label_value) {

					$label_name = $current_label_value->name;
					if(!isset($labels[$label_name])) {

						$all_langs_label = array_filter((array)$tool_object->labels, function($el) use($label_name) {
							return $el->name===$label_name;
						});
						foreach ($all_langs_label as $item) {
							if (!isset($item->lang)) {
								// ignore
								debug_log(__METHOD__
									. " Ignored item without expected property 'lang'. Check this tool definition labels " . PHP_EOL
									. ' item: ' . to_string($item) .PHP_EOL
									. ' all_langs_label: ' . to_string($all_langs_label) .PHP_EOL
									. ' tool_object: ' . to_string($tool_object)
									, logger::ERROR
								);
							}

							if ($item->lang===DEDALO_DATA_LANG) {
								$labels[$label_name] = $item;
								continue 2;
							}
						}

						// fallback lang. Get the first one as fallback value setting as lang current lang
						$fallback_label = reset($all_langs_label);
						$fallback_label->lang = DEDALO_DATA_LANG; // inject current lang to prevent find errors
						$labels[$label_name] = $fallback_label;
					}
				}

				// remove keys
				$labels = array_values($labels);
			}

		// properties
			$properties = empty($tool_object->properties)
				? null
				: $tool_object->properties; // object|array

		// config. Add if exists config data for current tool
			$ar_config		= tools_register::get_all_config_tool_client();
			$config_data	= array_find($ar_config, function($el) use($name) {
				return $el->name===$name;
			});

			if(empty($ar_config)){
				$ar_config		= tools_register::get_all_default_config_tool_client();
				$config_data	= array_find($ar_config, function($el) use($name) {
					return $el->name===$name;
				});
			}

		// lang
			$lang = DEDALO_DATA_LANG;

		// css
			$css = (object)[
				'url' => DEDALO_TOOLS_URL . '/' . $name . '/css/' .$name. '.css'
			];

		// icon
			$icon = DEDALO_TOOLS_URL . '/' . $name . '/img/icon.svg';

		// context
			$dd_object = new dd_object((object)[
				'name'				=> $name,
				'label'				=> $tool_label,
				'developer'			=> $developer[0] ?? null,
				'tipo'				=> $component_tipo,
				'section_tipo'		=> $tool_object->section_tipo,
				// 'section_id'		=> $tool_object->section_id,
				'model'				=> $name,
				'lang'				=> $lang,
				'mode'				=> 'edit',
				'properties'		=> $properties,
				'css'				=> $css,
				'icon'				=> $icon,
				'labels'			=> $labels,
				'description'		=> $description,
				'show_in_inspector'	=> $tool_object->show_in_inspector ?? null,
				'show_in_component'	=> $tool_object->show_in_component ?? null,
				'config'			=> !empty($config_data) ? $config_data->config : null
			]);


		return $dd_object;
	}//end get_structure_context



	/**
	* CREATE_TOOL_SIMPLE_CONTEXT
	* Parse a tool context from a simple_tool_object
	* @param object $tool_object (simple_tool_object from tool record JSON component dd1353)
	* @param object $tool_config (from properties)
	* @return dd_object $tool_simple_context
	*/
	public static function create_tool_simple_context(object $tool_object, object $tool_config=null, string $tipo=null, string $section_tipo=null) : dd_object {

		// old way. (!) Unification with context in progress..
			// label. (JSON list) Try match current lang else use the first lang value
				$tool_label = array_find($tool_object->label, function($el){
					return $el->lang===DEDALO_DATA_LANG;
				})->value ?? reset($tool_object->label)->value;
				if (empty($tool_label)) {
					$tool_label = $tool_object->name ?? 'Unknown';
				}

			// description. (text_area) Try match current lang else use the first lang value
				// $description = array_find((array)$tool_object->description, function($el){
				// 	return $el->lang===DEDALO_DATA_LANG;
				// })->value[0] ?? reset($tool_object->description)->value[0];

			// css
				$css = (object)[
					'url' => DEDALO_TOOLS_URL . '/' . $tool_object->name . '/css/' .$tool_object->name. '.css'
				];

			// icon
				$icon = DEDALO_TOOLS_URL . '/' . $tool_object->name . '/img/icon.svg';

			// developer
				$developer = isset($tool_object->developer[0])
					? ($tool_object->developer[0]->value[0] ?? null)
					: null;

			// context
				$tool_simple_context = new dd_object((object)[
					'name'				=> $tool_object->name,
					'label'				=> $tool_label,
					'developer'			=> $developer,
					// 'tipo'			=> $component_tipo,
					'section_tipo'		=> $tool_object->section_tipo,
					// 'section_id'		=> $tool_object->section_id,
					'model'				=> $tool_object->name,
					// 'lang'			=> $lang,
					'mode'				=> 'edit',
					'properties'		=> $tool_object->properties,
					'css'				=> $css,
					'icon'				=> $icon,
					// 'labels'			=> $labels,
					// 'description'	=> $description,
					'show_in_inspector'	=> $tool_object->show_in_inspector ?? null,
					'show_in_component'	=> $tool_object->show_in_component ?? null,
					// 'config'			=> !empty($config_data) ? $config_data->config : null
				]);

		// new way. (!) Unification with context in progress..
			// // short vars
			// 	$section_id		= $tool_object->section_id;
			// 	$section_tipo	= $tool_object->section_tipo;
			// 	$model			= $tool_object->name;

			// // tool construct and get JSON context
			// 	$element = new $model($section_id, $section_tipo);
			// 	// element JSON
			// 	$get_json_options = new stdClass();
			// 		$get_json_options->get_context	= true;
			// 		$get_json_options->get_data		= false;
			// 	$element_json = $element->get_json($get_json_options);
			// 	$context = $element_json->context;

			// // tool_simple_context
			// 	$tool_simple_context = $context;
			// 		// dump($tool_simple_context, ' ++ ============================== tool_simple_context ++ '.to_string($section_tipo.'-'.$section_id));
			// 		// dump($context, ' ++ ============================== context ++ '.to_string($section_tipo.'-'.$section_id));


		// tool_config add
			if (!empty($tool_config)) {
				// parse and resolve ddo_map self
					if (isset($tool_config->ddo_map)) {
						$tool_config->ddo_map = array_map(function($el) use($tipo, $section_tipo){
							if ($el->tipo==='self') {
								$el->tipo = $tipo;
							}
							if ($el->section_tipo==='self') {
								$el->section_tipo = $section_tipo;
							}
							if (!isset($el->model)) {
								$el->model = RecordObj_dd::get_modelo_name_by_tipo($el->tipo,true);
							}
							// check if the component is translatable and set to true or false
							$el->translatable = RecordObj_dd::get_translatable($el->tipo);

							$el->label = RecordObj_dd::get_termino_by_tipo($el->tipo, DEDALO_APPLICATION_LANG, true, true);

							return $el;
						}, $tool_config->ddo_map);
					}

				// set parsed tool_config
					$tool_simple_context->tool_config = $tool_config;
			}//end if (!empty($tool_config))



		return $tool_simple_context;
	}//end create_tool_simple_context



	/**
	* GET_REGISTERED_TOOLS
	* Get the full or filtered list data of current registered tools in database
	* @return array $registered_tools
	*/
	public static function get_client_registered_tools() : array {

		$registered_tools = [];

		static $registered_tools_cache;

		// cache
			$use_cache = true;
			if ($use_cache===true) {
				// static
					if (isset($registered_tools_cache)) {
						return $registered_tools_cache;
					}

				// session
					// if(isset($_SESSION['dedalo']['registered_tools'])) {
					// 	return $_SESSION['dedalo']['registered_tools'];
					// }

				// cache file
			 		$file_cache = dd_cache::cache_from_file((object)[
						'file_name'	=> 'cache_registered_tools.json'
					]);
					if (!empty($file_cache)) {
						// read from file encoded JSON
						$registered_tools = json_handler::decode($file_cache);

						// static save value
						$registered_tools_cache = $registered_tools;

						return $registered_tools;
					}
			}

		// client_registered_tools_records
			static $client_registered_tools_records;
			if(!isset($client_registered_tools_records)) {

				// get all active and registered tools
					$sqo_tool_active = json_decode('{
						"section_tipo": "'.DEDALO_REGISTER_TOOLS_SECTION_TIPO.'",
						"limit": 0,
						"filter": {
							"$and": [
								{
									"q": {"section_id":"1","section_tipo":"dd64","type":"dd151","from_component_tipo":"dd1354"},
									"q_operator": null,
									"path": [
										{
											"section_tipo": "'.DEDALO_REGISTER_TOOLS_SECTION_TIPO.'",
											"component_tipo": "dd1354",
											"model": "component_radio_button",
											"name": "Active"
										}
									]
								}
							]
						},
						"full_count": false
					}');
					$search	= search::get_instance($sqo_tool_active);
					$result	= $search->search();

					// fix cache static
					$client_registered_tools_records = $result->ar_records;
			}
		// get all tools config sections
			$ar_config = tools_register::get_all_config_tool_client();

		// get the simple_tool_object
			foreach ($client_registered_tools_records as $record) {

				$section		= section::get_instance($record->section_id, $record->section_tipo);
				$section_dato	= $record->datos;
				$section->set_dato($section_dato);

				// simple tool object 'dd1353'
				$component_tipo	= 'dd1353';
				$model			= RecordObj_dd::get_modelo_name_by_tipo($component_tipo,true);
				$component		= component_common::get_instance(
					$model,
					$component_tipo,
					$record->section_id,
					'list',
					DEDALO_DATA_NOLAN,
					$record->section_tipo
				);
				$dato = $component->get_dato();
				if (empty($dato)) {
					debug_log(__METHOD__
						." Ignored empty dato of  $record->section_tipo - $component_tipo - $record->section_id " . PHP_EOL
						.' model: ' . to_string($model)
						, logger::WARNING
					);
					continue;
				}

				$current_value = reset($dato);
				if(isset($ar_tools) && !in_array($current_value->name, $ar_tools)){
					continue;
				}

				// append config
					$current_config	= array_find($ar_config, function($el) use($current_value) {
						return $el->name===$current_value->name;
					});
					// $current_config = array_filter($ar_config, function($item) use($current_value){
					// 	if($item->name === $current_value->name) {
					// 		return $item;
					// 	}
					// });

					if(empty($current_config)){
						$ar_config		= tools_register::get_all_default_config_tool_client();
						$current_config	= array_find($ar_config, function($el) use($current_value) {
							return $el->name===$current_value->name;
						});
					}
					$current_value->config = !empty($current_config)
						? $current_config->config
						: null;

				$registered_tools[] = $current_value;
			}//end foreach ($client_registered_tools_records as $record)

		// cache
			if ($use_cache===true) {
				// static
					$registered_tools_cache = $registered_tools;

				// session
					// $_SESSION['dedalo']['registered_tools'] = $registered_tools;

				// cache file
					dd_cache::cache_to_file((object)[
						'data'		=> $registered_tools,
						'file_name'	=> 'cache_registered_tools.json'
					]);
			}


		return $registered_tools;
	}//end get_client_registered_tools



	/**
	* GET_CONFIG
	* Get given tool config if isset
	* @param string $tool_name
	* @return object|null $config
	*/
	public static function get_config(string $tool_name) : ?object {

		// get all tools config sections
			$ar_config = tools_register::get_all_config_tool();

		// select current from all tool config
			$config = array_find($ar_config, function($el) use($tool_name) {
				return $el->name===$tool_name;
			});

			if(empty($config)){
				// get all tools config sections
				$ar_config = tools_register::get_all_default_config();
				$config = array_find($ar_config, function($el) use($tool_name) {
					return $el->name===$tool_name;
				});
			}

		return $config;
	}//end get_config



	/**
	* READ_FILES
	* Read files from directory and return all files array filtered by extension
	* @return array $ar_data
	*/
	public static function read_files(string $dir, array $valid_extensions=['csv']) : array {

		$ar_data = array();

		// scan directory
			try {
				$root = is_dir($dir)
					? scandir($dir)
					: null;
			} catch (Exception $e) {
				debug_log(__METHOD__
					." Error on read dir: ".to_string($dir)
					, logger::ERROR
				);
			}

		// error on read the dir or empty result
			if (!$root) {
				return $ar_data; // empty array
			}

		// sort files in natural order
			natsort($root);

		// iterate and get only files. Skip others
			foreach($root as $value) {

				// Skip non valid extensions
					$file_parts = pathinfo($value);
					if(!isset($file_parts['extension']) || !in_array(strtolower($file_parts['extension']), $valid_extensions)) {
						debug_log(__METHOD__." Skipped file: $value", logger::DEBUG);
						continue;
					}

				// Case file
					if(is_file("$dir/$value")) {
						$ar_data[] = $value;
					}

				// Case dir recursive ($recursive=true)
					// if($recursive) foreach(self::find_all_files("$dir/$value", $recursive) as $value) {
					// 	$ar_data[] = $value;
					// }
			}

		// SORT ARRAY (By custom core function build_sorter)
			// usort($ar_data, build_sorter('numero_recurso'));


		return $ar_data;
	}//end read_files



	/**
	* READ_CSV_FILE_AS_ARRAY
	* Reads given csv file as array of data.
	* Note that expected encoding is UTF-8 and
	* the locale settings are taken into account by php fgetcsv function.
	* If LC_CTYPE is e.g. en_US.UTF-8, files in one-byte encodings may be read wrongly by fgetcsv.
	* When file encoding is different from UTF-8, a conversion try will be made.
	* @param string $file
	* @param bool $skip_header
	* @param string $csv_delimiter
	* @param string $enclosure
	* @param string $escape
	*
	* @return array $csv_array
	* 	An empty array is returned when something wrong happens, like when the file doesn't exist
	*/
	public static function read_csv_file_as_array(string $file, bool $skip_header=false, string $csv_delimiter=';', string $enclosure='"', string $escape='"') : array {

		// file not found case
			if(!file_exists($file)) {
				debug_log(__METHOD__
					." File not found " . PHP_EOL
					.' file: '.to_string($file)
					, logger::ERROR
				);
				return [];
			}

		// auto_detect_line_endings
			$is_php81 = (version_compare(PHP_VERSION, '8.1.0') >= 0);
			if (!$is_php81) {
				ini_set('auto_detect_line_endings', true);
			}

		// open file in read mode
			$f = fopen($file, "r");

		// read contents line by line and store data
			$csv_array			= array();
			$convert_to_utf8	= false;
			$bom				= pack('H*','EFBBBF');
			$i=0;
			while (($line = fgetcsv($f, 0, $csv_delimiter, $enclosure, $escape)) !== false) {

				// skip header case
					if ($skip_header && $i===0) {
						$i++;
						continue;
					}

				// safe array type
					if (!is_array($line)) {
						$line = [$line];
					}

				// encoding check . Only UFT-8 is valid. Another encodings will be converted to UTF-8
					// $sample = reset($line);
					$sample = is_array($line) ? implode(', ', $line) : (string)$line;
					if ($convert_to_utf8===true || !mb_check_encoding($sample, 'UTF-8')) {
						foreach ($line as $key => $current_value) {
							// $line[$key] = utf8_encode($current_value);
							// replacement for PHP8.2 (https://php.watch/versions/8.2/utf8_encode-utf8_decode-deprecated)
							// $line[$key] = mb_convert_encoding($current_value, 'UTF-8', 'ISO-8859-1'); // ISO-8859-1 to UTF-8
							$line[$key] = mb_convert_encoding($current_value, 'UTF-8', mb_list_encodings()); // Any encoding to UTF-8
						}
						$convert_to_utf8 = true; // prevent to check more than once
					}

				// iterate line cells (columns from split text line by $csv_delimiter)
					foreach ($line as $cell) {
						// remove BOM in the first line when is set.
						$cell_clean = $i===0
							? preg_replace("/^$bom/", '', $cell)
							: $cell;

						$csv_array[$i][] = trim($cell_clean);
					}

				$i++;
			}//end while

		// close file a end
			fclose($f);

		// auto_detect_line_endings
			if (!$is_php81) {
				ini_set('auto_detect_line_endings', false);
			}


		return $csv_array;
	}//end read_csv_file_as_array



	/**
	* CALL_COMPONENT_METHOD (NOT USED AT THE MOMENT)
	* Call component method
	* @param object $options
	* @return object $response
	*/
	public static function call_component_method(object $options) : object {

		// Working here... (!)
		throw new Exception("Error Processing Request", 1);


		$response = new stdClass();
			$response->result	= false;
			$response->msg		= 'Error. Request failed ['.__FUNCTION__.']';

		// options
			$tipo				= $options->tipo ?? null;
			$section_id			= $options->section_id ?? null;
			$section_tipo		= $options->section_tipo ?? null;
			$method				= $options->method ?? null;
			$method_arguments	= $options->method_arguments ?? null;

		// component
			$model		= RecordObj_dd::get_modelo_name_by_tipo($tipo,true);
			$component	= component_common::get_instance(
				$model,
				$tipo,
				$section_id,
				'list',
				DEDALO_DATA_NOLAN,
				$section_tipo
			);
			if (!empty($method) && method_exists($component, $method)) {

				// call component
					$call_result = $component->{$method}($method_arguments);

				// response
					$response->result = isset($call_result->result)
						? $call_result->result
						: $call_result;
					$response->msg = isset($call_result->msg)
						? $call_result->msg
						: 'Request done ['.__FUNCTION__.']';

			}else{

				// response error
					$response->result	= false;
					$response->msg		.= '. Method does not exists: '.$method .' in '.$model;
			}


		return $response;
	}//end call_component_method



	/**
	* GET_USER_TOOLS
	* Get filtered user authorized tools
	* (Filtered by profiles security_tools data)
	* @param int $user_id
	* @return array $user_tools
	*/
	public static function get_user_tools(int $user_id) : array {

		$user_tools = [];

		// empty or zero user case
			if (empty($user_id)) {
				return $user_tools;
			}

		// cache
			$use_cache = true;
			static $cache_user_tools;
			if ($use_cache===true) {
				if (isset($cache_user_tools[$user_id])) {
					return $cache_user_tools[$user_id];
				}
			}

		// all unfiltered tools
			$registered_tools = tool_common::get_client_registered_tools();

		// user_tools
			if ($user_id==DEDALO_SUPERUSER) {

				$user_tools = $registered_tools;

			}else{

				// tool permissions (DEDALO_COMPONENT_SECURITY_TOOLS_PROFILES_TIPO)
					$security_tools_dato = (function() use($user_id) {

						$user_profile = security::get_user_profile($user_id);
						if (empty($user_profile)) {
							return []; // empty array
						}
						$user_profile_id		= (int)$user_profile->section_id;
						$security_tools_model	= RecordObj_dd::get_modelo_name_by_tipo(DEDALO_COMPONENT_SECURITY_TOOLS_PROFILES_TIPO, true);
						$component				= component_common::get_instance(
							$security_tools_model,
							DEDALO_COMPONENT_SECURITY_TOOLS_PROFILES_TIPO,
							$user_profile_id,
							'list',
							DEDALO_DATA_NOLAN,
							DEDALO_SECTION_PROFILES_TIPO
						);
						// dato
						return $component->get_dato();
					})();

				// allowed tools
					$ar_allowed_id = array_map(function($el){
						return $el->section_id;
					}, $security_tools_dato);

				// filter user authorized tools
					foreach ($registered_tools as $tool) {

						if( (isset($tool->always_active) && $tool->always_active===true) ||
							 in_array($tool->section_id, $ar_allowed_id)
							) {
							$user_tools[] = $tool;
						}
					}
			}

		// cache
			if ($use_cache===true) {
				$cache_user_tools[$user_id] = $user_tools;
			}

		// debug
			// $names = array_map(function($el){
			// 	return $el->name .' - '. $el->section_id;
			// }, $user_tools);
			// dump($names, ')))))))))))) $names ++ '.to_string());


		return $user_tools;
	}//end get_user_tools



}//end class tool_common
